package com.ziauddinmasoomi.webviewhomwork;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button go,back,next,refresh,clear;
    private WebView webView;
    private EditText ulrSearch;
    private String url;
    private WebSettings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initailizing();
    }


    @Override
    protected void onStart() {
        super.onStart();
        settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        go.setOnClickListener(this);
        back.setOnClickListener(this);
        next.setOnClickListener(this);
        refresh.setOnClickListener(this);
        clear.setOnClickListener(this);
    }

    public void onClick(View v){

        url = ulrSearch.getText().toString();

        switch (v.getId()){

            case R.id.goBtn:
                webView.loadUrl(url);
                break;
            case R.id.backwardBtn:
               if ( webView.canGoBack()) webView.goBack();
               else Toast.makeText(MainActivity.this,"There is no page to go back!",Toast.LENGTH_SHORT).show();
                break;
            case R.id.forwardBtn:
                if (webView.canGoForward()) webView.goForward();
                else Toast.makeText(MainActivity.this,"There is no page to go forward!",Toast.LENGTH_SHORT).show();
                break;
            case R.id.refreshBtn:
                webView.reload();
                break;
            case R.id.clearHBtn:
                webView.clearHistory();
                Toast.makeText(MainActivity.this,"The history was cleared!",Toast.LENGTH_SHORT).show();
                break;
        }

    }

    private void initailizing(){
        go = (Button) findViewById(R.id.goBtn);
        back = (Button) findViewById(R.id.backwardBtn);
        next = (Button) findViewById(R.id.forwardBtn);
        refresh = (Button) findViewById(R.id.refreshBtn);
        clear = (Button) findViewById(R.id.clearHBtn);
        ulrSearch = (EditText) findViewById(R.id.urlTextView);
        webView = (WebView) findViewById(R.id.webViewPart);
        webView.setWebViewClient(new WebViewClient());
    }
}
